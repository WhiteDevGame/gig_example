<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="contacto")
 */
class Contacto {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contacto", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idContacto;

    /**
     * @var \string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var \string
     *
     * @ORM\Column(name="lastName", type="string", nullable=false)
     */
    private $lastName;

    /**
     * @var \string
     *
     * @ORM\Column(name="email", type="string", nullable=false)
     */
    private $email;

    /**
     * @var \string
     *
     * @ORM\Column(name="country", type="string", nullable=false)
     */
    private $country;

    
    function getIdContacto() {
        return $this->idContacto;
    }

    function getName(){
        return $this->name;
    }

    function getLastName(){
        return $this->lastName;
    }

    function getEmail(){
        return $this->email;
    }

    function getCountry(){
        return $this->country;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setCountry($country) {
        $this->country = $country;
    }

}
