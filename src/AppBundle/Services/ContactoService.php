<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

class ContactoService {

    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function getListadoContactos() {
        //entidad Contacto no creada por lo que no 
        return $this->em->getRepository('AppBundle:Contacto')->findBy(array(), array('lastName' => 'ASC'));
    }

    public function borrarContacto($oContacto) {
        $this->em->remove($oContacto);
        $this->em->flush();
    }

    public function guardarContacto($request, $oContacto) {
        try {
            if (is_null($oContacto)) {
                $oContacto = new \AppBundle\Entity\Contacto();
            }
            $oContacto->setName($request->get('name'));
            $oContacto->setLastName($request->get('lastName'));
            $oContacto->setEmail($request->get('email'));
            $oContacto->setCountry($request->get('country'));

            $this->em->persist($oContacto);
            $this->em->flush();
            return true;
        } catch (\Exception $exc) {
            return false;
        }
    }

//    public function borrarEjercicio($oEjercicio) {
//        try {
//            $this->em->remove($oEjercicio);
//            $this->em->flush();
//            return true;
//        } catch (\Exception $exc) {
//            return false;
//        }
//    }
}
