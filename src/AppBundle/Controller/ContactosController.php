<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\ContactoService;
use AppBundle\Entity\Contacto;

/**
 * Description of ContactosController
 *
 * @author JosePC
 */
class ContactosController extends Controller {

    /**
     * @Route("/listadoContactos", name="listadoContactos")
     */
    public function listadoContactosAction(Request $request, ContactoService $contactoService) {
        $aListadoContactos = $contactoService->getListadoContactos();

        return $this->render('AppBundle:Contacto:listado.html.twig', [
                    "contactos" => $aListadoContactos
        ]);
    }

    /**
     * @Route("/detalleContacto/{idContacto}", name="contacto_detalle")
     * @ParamConverter("oContacto", class="AppBundle:Contacto", options={"id" = "idContacto"})
     */
    public function detalleContactoAction(Request $request, Contacto $oContacto = null, ContactoService $contactoService) {
        $aListadoContactos = $contactoService->getListadoContactos();

        if (is_null($oContacto)) {
            $oContacto = new Contacto();
        }
//        dump($oContacto);
//        die;

        return $this->render('AppBundle:Contacto:detalleContacto.html.twig', [
                    "contacto" => $oContacto
        ]);
    }

    /**
     * @Route("/guardar/{idContacto}", name="guardar_contacto")
     * @ParamConverter("oContacto", class="AppBundle:Contacto", options={"id" = "idContacto"})
     */
    public function guardarContactoAction(Request $request, Contacto $oContacto = null, ContactoService $contactoService) {

        if (is_null($oContacto)) {
            $oContacto = new Contacto();
        }
        $contactoService->guardarContacto($request, $oContacto);

        return $this->redirectToRoute('listadoContactos');
    }

    /**
     * @Route("/borrarContacto/{idContacto}", name="borrarContacto")
     * @ParamConverter("oContacto", class="AppBundle:Contacto", options={"id" = "idContacto"})
     */
    public function borrarContactoAction(Request $request, Contacto $oContacto = null, ContactoService $contactoService) {
        $aListadoContactos = $contactoService->borrarContacto($oContacto);

        return $this->redirectToRoute('listadoContactos');
    }

}
